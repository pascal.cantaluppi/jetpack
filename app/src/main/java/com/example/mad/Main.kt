package com.example.mad
import DetailScreen
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.mad.ui.theme.MadTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MadTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    var currentScreen by remember { mutableStateOf<String?>(null) }

                    if (currentScreen == null) {
                        MyScreen { selectedItem ->
                            currentScreen = selectedItem
                        }
                    } else {
                        DetailScreen(item = currentScreen!!) {
                            currentScreen = null
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun MyScreen(onItemClick: (String) -> Unit) {
    var searchText by remember { mutableStateOf("") }
    val allItems = listOf("Basel SBB", "Ludwigshafen (Rhein)", "Mannheim")
    var filteredItems by remember { mutableStateOf(allItems) }

    fun filterList() {
        filteredItems = if (searchText.isBlank()) {
            allItems
        } else {
            allItems.filter { it.contains(searchText, ignoreCase = true) }
        }
    }

    Column {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            OutlinedTextField(
                value = searchText,
                onValueChange = { searchText = it },
                label = { Text("Suchen") },
                modifier = Modifier.weight(1f)
            )
            Button(
                onClick = { filterList() },
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(start = 8.dp)
            ) {
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.baseline_search_24),
                    contentDescription = "Suchen"
                )
            }
        }

        LazyColumn(modifier = Modifier.padding(8.dp)) {
            items(filteredItems) { item ->
                ListItem(item, onItemClick)
            }
        }
    }
}

@Composable
fun ListItem(item: String, onItemClick: (String) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp)
        .clickable { onItemClick(item) },
        shape = RoundedCornerShape(8.dp),
        colors = CardDefaults.cardColors(containerColor = Color.LightGray)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(16.dp)
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.baseline_directions_transit_24),
                contentDescription = "Zug Icon",
                modifier = Modifier.size(24.dp)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = item)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MyScreenPreview() {
    MadTheme {
        MyScreen(onItemClick = { })
    }
}

