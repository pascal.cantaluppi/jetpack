# Wireframe-Screen in Android App integrieren

## Auftrag

Implementieren Sie einen Screen Ihres Wireframes mit Jetpack Compose

- Wählen Sie dafür einen Screen aus, der möglichst spezifisch für Ihre App ist
- Nutzen Sie Jetpack Compose, um die UI-Komponenten zu implementieren
- Verwenden Sie das Codelab Implement a real-world design für konkrete Implementierungsbeispiele
- Führen Sie die App anschliessend im Android Emulator aus